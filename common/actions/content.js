import { addItem, removeItem, saveItem } from '../api/content.js';
import { SET_META, setActive } from './tree.js';

export const ADD_NODE_SUCCESS = 'ADD_NODE_SUCCESS';
export const NODE_SAVED = 'NODE_SAVED';
export const NODE_DELETED = 'NODE_DELETED';
export const CONTENT_CHANGE = 'CONTENT_CHANGE';
export const NAME_CHANGE = 'NAME_CHANGE';

export const addFolder = () => async (dispatch, getState) => {
  const name = prompt('Enter folder name');
  if (!name) return;
  const { _id } = getState().tree.active;
  const newNode = { name, type: 'folder', parentId: _id };
  const response = await addItem(newNode);

  if (response.status === 200) {
    const { insertedId } = await response.json();
    dispatch({type: ADD_NODE_SUCCESS, node: { ...newNode, _id: insertedId }});
    const { size, count } = getState().content;
    dispatch({ type: SET_META, size, count: count+1 });
  }
}

export const addFile = () => async (dispatch, getState) => {
  const name = prompt('Enter file name');
  if (!name) return;
  const { _id } = getState().tree.active;
  const newNode = { name, type: 'file', parentId: _id };
  const response = await addItem(newNode);

  if (response.status === 200) {
    const { insertedId } = await response.json();
    dispatch({type: ADD_NODE_SUCCESS, node: { ...newNode, _id: insertedId }});
    const { size, count } = getState().content;
    dispatch({ type: SET_META, size, count: count+1 });
  }
}

export const save = (node) => async(dispatch) => {
  const response = await saveItem(node);
  if(response.status === 200) {
    dispatch({
      type: NODE_SAVED,
      node
    })
  }
}

export const deleteItem = (id) => async(dispatch) => {
  const response = await removeItem(id);
  if(response.status === 200) {
    dispatch({
      type: NODE_DELETED,
      id
    });
    dispatch(setActive({}));
  }
}


export const contentChange = ({target}) => ({
  type: CONTENT_CHANGE,
  value: target.value
});

export const nameChange = ({target}) => ({
  type: NAME_CHANGE,
  value: target.value
})
