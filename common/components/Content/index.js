import React, {Component} from 'react'
import { connect } from 'react-redux';
import { addFolder, addFile, save, deleteItem, contentChange, nameChange } from '../../actions/content.js';
import { bindActionCreators } from 'redux';
import cx from 'classnames';
import Folder from './Folder.js';
import File from './File.js';

const stateToProps = (state) => ({
  active: state.tree.active,
  name: state.content.name,
  content: state.content.content,
  count: state.content.count,
  size: state.content.size,
  loading: state.content.loading
})

const mapDispatchToProps = (dispatch) => ({
  dispatch,
  addFolder: bindActionCreators(addFolder, dispatch),
  addFile: bindActionCreators(addFile, dispatch),
  save: bindActionCreators(save, dispatch),
  deleteItem: bindActionCreators(deleteItem, dispatch),
  contentChange: bindActionCreators(contentChange, dispatch),
  nameChange: bindActionCreators(nameChange, dispatch)
})

@connect(stateToProps,mapDispatchToProps)
class Content extends Component {

  onSave = () => {
    const { name, content, active, save } = this.props;
    const { _id } = active;
    save({ name, content, _id })
  }

  onDelete = () => {
    const { active, deleteItem } = this.props;
    const { _id } = active;
    deleteItem(_id);
  }

  render () {
    const { active } = this.props;

    switch (active.type) {
      case 'root':
      case 'folder':
        return <Folder {...this.props} onSave={this.onSave} onDelete={this.onDelete} />
      case 'file':
        return <File {...this.props} onSave={this.onSave} onDelete={this.onDelete} />
      default:
        return null;

    }
  }
}

export default Content;
