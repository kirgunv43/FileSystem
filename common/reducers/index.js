import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import {reducer as formReducer} from 'redux-form';
import content from './content';
import tree from './tree';

const rootReducer = combineReducers({
  routing: routerReducer,
  form: formReducer,
  content,
  tree
})

export default rootReducer;
