/* eslint-disable no-console, no-use-before-define */

import path from 'path'
import Express from 'express'
import qs from 'qs'

import webpack from 'webpack'
import webpackDevMiddleware from 'webpack-dev-middleware'
import webpackHotMiddleware from 'webpack-hot-middleware'
import webpackConfig from '../webpack.config'

import React from 'react'
import { renderToString } from 'react-dom/server'
import { Provider } from 'react-redux'
import serialize from 'serialize-javascript'
import configureStore from '../common/store/configureStore'
import { createMemoryHistory, match, RouterContext } from 'react-router'
import routes from '../common/routes'
import { syncHistoryWithStore } from 'react-router-redux'
import { MongoClient, ObjectID } from 'mongodb';
import bodyParser from 'body-parser';
const app = new Express()
const port = 3000

// Use this middleware to set up hot module reloading via webpack.
const compiler = webpack(webpackConfig);
app.use(webpackDevMiddleware(compiler, { noInfo: true, publicPath: webpackConfig.output.publicPath }));
app.use(webpackHotMiddleware(compiler));
app.use(bodyParser.json());

const HTML = ({ content, store }) => (
 <html>
   <head>
    <link rel='stylesheet' href='/static/bundle.css'/>
   </head>
   <body>
     <div id='root' dangerouslySetInnerHTML={{ __html: content }} />
     <script dangerouslySetInnerHTML={{ __html: `window.__initialState__=${serialize(store.getState())};` }}/>
     <script src='/static/bundle.js'/>
   </body>
 </html>
);

var url = 'mongodb://user:a1b2c3D4@ds011449.mlab.com:11449/soul';
MongoClient.connect(url, async function(err, db) {
  console.log(err);
  if (!err) {
    console.log('Connected correctly to server');
    let tree = db.collection('tree');
    const root = await tree.findOne({ type: 'root', name: 'Root' });
    if (!root) {
      tree.insert( { type: 'root', name: 'Root' } );
    }
    db.close();
  } else {
    console.log(err);
  }
});

app.use('/static', Express.static('static'));

app.get('/api/v1/content/meta/:id', async (req, res) => {
  const { id } = req.params;
  const db = await MongoClient.connect(url);
  const tree = db.collection('tree');
  const childs = await tree.find({ parentId: id.toString()}).toArray();
  const size = await deepSize(tree, id);

  console.log(childs);
  console.log({size, count: childs.length});
  db.close();
  res.send(JSON.stringify({size, count: childs.length}));

});

async function deepSize(tree, id) {
  const node = await tree.findOne({ _id: new ObjectID(id) });
  const childs = await tree.find({ parentId: id.toString() }).toArray();
  let count = 0;
  if (childs.length) {
    for (let child of childs) {
      if(child.type === 'file') {
       count += (child.content || '').length;
     } else {
       count += await deepSize(tree, child._id);
     }
    }
  }
  return count;
}

app.get('/api/v1/content/get/:id', async (req, res) => {
  const { id } = req.params;
  const db = await MongoClient.connect(url);
  const tree = db.collection('tree');
  const node = await tree.findOne({ _id: new ObjectID(id) });
  res.send({content: node.content});
  db.close();
});

app.put('/api/v1/content/put', async (req, res) => {
  const { _id, content, name } = req.body;
  const db = await MongoClient.connect(url);
  const tree = db.collection('tree');
   let node = await tree.findOne({ _id: new ObjectID(_id) });
   node.content = content;
   node.name = name;
  tree.updateOne({ _id: new ObjectID(_id) }, node);
  res.sendStatus(200);
  db.close();
});

app.get('/api/v1/root', async (req, res) => {
  const db = await MongoClient.connect(url);
  const tree = db.collection('tree');
  const node = await tree.findOne({ type: 'root' });
  const childs = await tree.find({ parentId: node._id.toString() }).toArray();
  db.close();
  res.send(JSON.stringify({...node, childs, synchronized: true}));
});

app.get('/api/v1/childs/get/:parentId', async (req, res) => {
  const { parentId } = req.params;
  const db = await MongoClient.connect(url);
  const tree = db.collection('tree');
  const childs = await tree.find({ parentId: parentId.toString() }).toArray();
  res.send({childs});
  db.close();
});

app.post('/api/v1/childs/add', async (req, res) => {
  const { parentId } = req.body;
  const db = await MongoClient.connect(url);
  const tree = db.collection('tree');
  const parent = await tree.findOne({ _id: new ObjectID(parentId) });
  if (!parent) {
    res.sendStatus(500);
    return;
  }
  const response = await tree.insertOne( { ...req.body, parentId } );
  db.close();
  res.send(JSON.stringify({insertedId: response.insertedId}));
});

app.delete('/api/v1/childs/delete/:id', async (req, res) => {
  const { id } = req.params;
  const db = await MongoClient.connect(url);
  const tree = db.collection('tree');
  const result = await deepDelete(tree, id);
  db.close();
  res.sendStatus(200);
});

async function deepDelete(tree, id) {
  const node = await tree.findOne({ _id: new ObjectID(id) });
  const childs = await tree.find({ parentId: node._id.toString() }).toArray();
  if (childs.length) {
    for (let child of childs) {
      const result = await deepDelete(tree, child._id);
    }
  }
  tree.remove({ _id: new ObjectID(id)});
  return null;
}

app.use(function (req, res) {
  const memoryHistory = createMemoryHistory(req.url);
  const store = configureStore();
  const history = syncHistoryWithStore(memoryHistory, store);
  match({ history, routes, location: req.url }, (error, redirectLocation, renderProps) => {
    if (error) {
      res.status(500).send(error.message)
    } else if (redirectLocation) {
      res.redirect(302, redirectLocation.pathname + redirectLocation.search)
    } else if (renderProps) {
      const content = renderToString(
        <Provider store={store}>
          <RouterContext {...renderProps}/>
        </Provider>
      )
      res.send('<!doctype html>\n' + renderToString(<HTML content={content} store={store}/>))
    }
  })
})

app.listen(port, (error) => {
  if (error) {
    console.error(error)
  } else {
    console.info(`==> 🌎  Listening on port ${port}. Open up http://localhost:${port}/ in your browser.`)
  }
})
