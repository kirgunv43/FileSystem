import React, {Component} from 'react'
import { connect } from 'react-redux';
import { setActive } from '../../actions/tree.js';
import { bindActionCreators } from 'redux';
import cx from 'classnames';

class File extends Component {
  render () {
    const { name, content, addFolder, onSave, onDelete, nameChange, contentChange, size  } = this.props;
    return(
      <div className='content__container'>
        <div className='content_header'>
          <div className='content_header__file_name'>
            <input value={name} onChange={nameChange}/>
          </div>
          <div className='content_header__operations'>
            <div className='button__container'>
              <button onClick={onSave} className='button button__save'/>
              <button onClick={onDelete} className='button button__delete'/>
            </div>
          </div>
        </div>
        <div className='content__meta'>
          <span>size: {(size || 0).toString()}</span>
        </div>
        <div className='content__text'>
          <textarea value={content} onChange={contentChange}/>
        </div>
      </div>
    );
  }
}

export default File;
