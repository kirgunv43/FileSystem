import React from 'react'
import { Provider } from 'react-redux'
import { Router, Route, IndexRoute } from 'react-router';
import BaseLayout from '../layouts/BaseLayout';
import NotFound from '../layouts/NotFound';
import App from '../components/App';
const routes = (
    <Route path="/" component={BaseLayout}>
      <IndexRoute component={App}/>
      <Route path="*" component={NotFound}/>
    </Route>);


export default routes;
