import React, {Component} from 'react';
import Node from './Node';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { requestRoot } from '../actions/tree.js';

function mapStateToProps(state) {
  return { tree: state.tree.tree };
}

function mapDispatchToProps(dispatch) {
  return { requestRoot: bindActionCreators(requestRoot, dispatch) };
}

@connect(mapStateToProps, mapDispatchToProps)
class Tree extends Component {

  componentWillMount() {
    this.props.requestRoot();
  }

  render () {
    const {tree} = this.props;
    return(
      <ul className='root_tree'>
        <Node node={tree} />

      </ul>);
  }
}


export default Tree;
