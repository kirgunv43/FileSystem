import { ROOT_LOADED, SET_ACTIVE, TRIGER_FOLDER_CONTENT_VISIBLE, CHILDS_LOADED } from '../actions/tree.js';
import { ADD_NODE_SUCCESS, NODE_DELETED, NODE_SAVED } from '../actions/content.js';

function tree (state = {tree:{}, active:{}}, action) {
  switch (action.type) {
    case ROOT_LOADED: {
      return { ...state, tree: action.root };
    }
    case SET_ACTIVE: {
      return { ...state, active: action.node };
    }
    case TRIGER_FOLDER_CONTENT_VISIBLE: {
      return {
        ...state,
        active: action.node,
        tree: changeFolderContentVisible(state.tree, action.node)
      };
    }
    case CHILDS_LOADED: {
      return {
        ...state,
        tree: childsLoaded(state.tree, action.nodeId, action.childs)
      };
    }
    case ADD_NODE_SUCCESS: {
      return {...state, tree: addNode(state.tree, action.node)}
    }
    case NODE_DELETED: {
      return {...state, tree: deleteNode(state.tree, action.id)}
    }
    case NODE_SAVED: {
      return {...state, tree: changeNode(state.tree, action.node)}
    }
    default:
      return {...state};
  }
}

function changeNode (tree, node) {
  const sresult = findNodeById(tree, node._id);
  if(!sresult) {
    return {...tree};
  }
  console.log(`Before: ${sresult}`);
  console.log(`NEW: ${node}`);
  sresult.name = node.name;
  sresult.content = node.content;
  console.log(`After: ${sresult}`);
  return {...tree};
}

function deleteNode(tree, id) {
  const node = findNodeById(tree, id);
  const parent = findNodeById(tree, node.parentId);

  if(!node || !parent) {
    return {...tree};
  }

  parent.childs = parent.childs.filter(item => item._id !== id);
  return {...tree};
}

function childsLoaded (tree, nodeId, childs) {
  const result = findNodeById(tree, nodeId);
  if(!result) {
    return {...tree};
  }

  result.childs = childs;
  result.synchronized = true;
  return {...tree};
}

function changeFolderContentVisible(tree, node) {
  const result = findNodeById(tree, node._id);
  if(!result) {
    return {...tree};
  }

  result.open = !result.open;
  return {...tree};
}

function addNode(tree, node) {
  const parent = findNodeById(tree, node.parentId);
  if(!parent) {
    return {...tree};
  }
  if (!parent.childs) {
    parent.childs = [];
  }

  parent.childs.push(node)
  return {...tree};
}

function findNodeById(node, id) {
  if (node._id === id) {
    return node;
  }
  if (!node.childs)  {
    return null;
  }
  for (var child of node.childs) {
    const searchResult = findNodeById(child, id);
    if (searchResult) {
      return searchResult;
    }
  }

  return null;
}


export default tree;
