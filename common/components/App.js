import React, {Component} from 'react';
import Content from './Content';
import Tree from './Tree';

class App extends Component {
  render () {
    return(
      <div className='container'>
        <div className='container__tree'>
          <Tree/>
        </div>
        <div className='container__content'>
          <Content/>
        </div>
      </div>);
  }
}


export default App;
