import fetch from 'isomorphic-fetch';

export const addItem = (node) => {
  return fetch(`/api/v1/childs/add`, { method: 'POST', body: JSON.stringify(node), headers: {'Content-Type' : 'application/json'}});
}

export const removeItem = (id) => {
  return fetch(`/api/v1/childs/delete/${id}`, { method: 'DELETE' });
}

export const saveItem = (node) => {
  return fetch(`/api/v1/content/put`, { method: 'PUT', body: JSON.stringify(node), headers: {'Content-Type' : 'application/json'}});
}

export const meta = (id) => {
  return fetch(`/api/v1/content/meta/${id}`);
}
