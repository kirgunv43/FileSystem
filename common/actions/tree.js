import { getRoot, getChilds } from '../api/tree.js';
import { meta } from '../api/content.js';

export const ROOT_LOADED = 'ROOT_LOADED';
export const SET_ACTIVE = 'SET_ACTIVE';
export const TRIGER_FOLDER_CONTENT_VISIBLE = 'TRIGER_FOLDER_CONTENT_VISIBLE';
export const CHILDS_LOADED = 'CHILDS_LOADED';
export const SET_META = 'SET_META';

export const requestRoot = () => async (dispatch) => {
  const response = await getRoot();
  const root = await response.json();
  dispatch({ type: ROOT_LOADED, root });
}

export const changeFolderContentVisible = (node) => async (dispatch) => {
  dispatch ({
    type: TRIGER_FOLDER_CONTENT_VISIBLE,
    node
  });

  if (node.childs) {
    for (let child of node.childs) {
      if (!child.synchronized) {
        dispatch(synchronizeChild(child._id));
      }
    }
  }
}

const synchronizeChild = (id) => async (dispatch) => {
  const response = await getChilds(id);
  const { childs } = await response.json();
  dispatch(childsLoaded(id, childs));
}

export const childsLoaded = (nodeId, childs) => {
  return {
    type: CHILDS_LOADED,
    nodeId,
    childs
  }
}

export const setActive = (node) => async (dispatch) => {
  dispatch ({
    type: SET_ACTIVE,
    node
  });
  dispatch({type: SET_META, size:0, count:0, loading: true});

  if (node.type !== 'file') {
    dispatch(updateMeta(node._id));
  } else {
    dispatch({ type: SET_META, size: (node.content || '').length })
  }
}

const updateMeta = (_id) => async (dispatch) => {
  const response = await meta(_id);
  const { size, count } = await response.json();
  console.log({ size: size || '0', count: count || '0' });
  dispatch({type: SET_META, size, count, loading: false})
}
