import fetch from 'isomorphic-fetch';

export const getRoot = () => {
  return fetch('/api/v1/root');
}

export const getChilds = (parentId) => {
  return fetch(`/api/v1/childs/get/${parentId}`);
}
