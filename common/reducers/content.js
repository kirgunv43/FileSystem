import { SET_ACTIVE, SET_META } from '../actions/tree.js';
import { CONTENT_CHANGE, NAME_CHANGE } from '../actions/content.js';

function content (state = {count: 0, size: 0}, action) {
  switch (action.type) {
    case SET_ACTIVE: {
      return {
        ...state,
        content: (action.node || {}).content,
        name: (action.node || {}).name
      }
    }
    case SET_META: {
      return {...state, count: action.count, size: action.size, loading: action.loading }
    }
    case CONTENT_CHANGE: {
      return {...state, content: action.value, size: action.value.length }
    }
    case NAME_CHANGE: {
      return {...state, name: action.value }
    }
    default:
      return {...state};
  }
}


export default content;
