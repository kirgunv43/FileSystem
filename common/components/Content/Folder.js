import React, {Component} from 'react'
import { connect } from 'react-redux';
import { setActive } from '../../actions/tree.js';

import cx from 'classnames';

class Folder extends Component {
  render () {
    const { name, content, addFolder, addFile, onSave, onDelete, nameChange, size, count, active, loading } = this.props;
    return(
      <div className='content__container'>
        <div className='content_header'>
          <div className='content_header__file_name'>
            <input value={name} onChange={nameChange}/>
          </div>
          <div className='content_header__operations'>
            <div className='button__container'>
              <button onClick={onSave} className='button button__save'/>
              { active.type !== 'root' && <button onClick={onDelete} className='button button__delete'/>}
              <button onClick={addFile} className='button button__add_file'/>
              <button onClick={addFolder} className='button button__add_folder'/>
            </div>
          </div>
        </div>
        <div className='content__meta'>
          {loading? <span>Loading...</span> : <span>items: {(count || 0).toString()}, size: {(size || 0).toString()}</span>}
        </div>
      </div>
    );
  }
}

export default Folder;
