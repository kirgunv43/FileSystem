import React, {Component} from 'react'
import { connect } from 'react-redux';
import { setActive, changeFolderContentVisible } from '../../actions/tree.js';
import { bindActionCreators } from 'redux';
import cx from 'classnames';

const stateToProps = (state) => ({
  active: state.tree.active,
})

const mapDispatchToProps = (dispatch) => ({
  dispatch,
  setActive: bindActionCreators(setActive, dispatch),
  changeFolderContentVisible: bindActionCreators(changeFolderContentVisible, dispatch)
})

@connect(stateToProps,mapDispatchToProps)
class Node extends Component {

  setActive = () => {
    if(this.props.node._id !== this.props.active._id){
      this.props.setActive(this.props.node);
    }
  }

  onOpen = () => {
    if (this.props.node.type !== 'file') {
      this.props.changeFolderContentVisible(this.props.node);
    }
  }

  getChilds() {
    if (!this.props.node.childs) {
      return null;
    }
    const child = this.props.node.childs.map(item => (<Node key={item._id} {...this.props} node={item} />));
    return (<ul className='root_tree'>{child}</ul>);
  }

  renderImage() {
    const { node } = this.props;
    switch (node.type) {
      case 'root':
      case 'folder': {
        const imageClass = cx('node-image__folder', {
          'node-image__folder--open': node.open,
          'node-image__folder--close': !node.open
        })
        return (<div className={imageClass} />)
      }
      case 'file':
        return (<div className='node-image__file' />)
    }
  }

  render () {
    const { node, active } = this.props;
    const nodeClassNames = cx('node__content' , { 'node--active': node._id === active._id });
    return (
      <li className='node'>
        <div className={nodeClassNames} onClick={this.setActive} onDoubleClick={this.onOpen}>
          <div className='node-image__container'>
            {this.renderImage()}
          </div>
          <span className='node__label'>{node.name}</span>
        </div>

         {node.open && this.getChilds()}
      </li>
    )
  }
}

export default Node;
